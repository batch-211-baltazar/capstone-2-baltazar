/*
Option 2: Data Model Requirements

Order - User - Product

Order{
	userId - ObjectId,
	products: [
	{
		productid,
		quantity
	
	}],
	totalAmount,
	purchasedOn
}


User{
	email,
	password,
	isAdmin
}

Product{
	name,
	description,
	price,
	isActive,
	createdOn
}


*/

//FROM RESOURCES COPY

/*

Type: E Commerce API

3-Model Structure

User
firstName - string,
lastName - string,
email - string,
password - string,
mobileNo - string,
isAdmin - boolean,
	      default: false


Product
name - string,
description - string,
price - number
isActive - boolean
		   default: true,
createdOn - date
			default: new Date()
orders - [
	{
		orderId - string,
		quantity - number

	}
]


Order
userId - string
totalAmount - number,
purchasedOn - date
		     default: new Date(),
products - [

	{
		productId - string,
		quantity - number
	}

]


2-Model Structure

User
firstName - string,
lastName - string,
email - string,
password - string,
mobileNo - string,
isAdmin - boolean,
	      default: false
orders: [
	
	{
		totalAmount - number,
		purchasedOn - date
				     default: new Date(),
		products - [

			{
				productId - string,
				quantity - number
			}

		]
	}

]

Product
name - string,
description - string,
price - number
isActive - boolean
		   default: true,
createdOn - date
			default: new Date()
orders: [
	
	{	
		orderId - string,
		userId - string,
		quantity - number,
		purchasedOn - date
				     default: new Date(),
	}

]

*/