const express = require("express");
const mongoose = require("mongoose")
const app = express();
const cors = require("cors");


const DOTENV_CONFIG_PATH='./.env.local'
require('dotenv').config({path:DOTENV_CONFIG_PATH});

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");



mongoose.connect(process.env.DB_URL,{
	useNewUrlParser:true,
	useUnifiedTopology: true
});

mongoose.connection.once('open',()=>console.log("Now connected to MongoDB Atlas."))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users",userRoutes);
app.use("/products",productRoutes);

// console.log(process.env.REACT_APP_API_URL)


app.listen(process.env.PORT || 3000,()=>{
	console.log(`API is now online on port ${process.env.PORT||3000}`)
})