const Product = require("../models/Product");
const User = require("../models/User");

// Create Product (Admin Only)

module.exports.createProduct = (data) =>{
	let newProduct = new Product({
		name : data.product.name,
		description : data.product.description,
		price : data.product.price
	})

	return newProduct.save().then((product,error)=>{
		if(error){
			return false;
		}else{
			return { message: "Item successfully added!" };
		};
	});
};

// module.exports.createProduct = (reqBody, isAdmin) => {
//   if (isAdmin === true) {
//     let newProduct = new Product({
//       name: reqBody.name,
//       description: reqBody.description,
//       price: reqBody.price,
//     });

//     return newProduct.save().then((result, error) => {
//       return error ? false : 'Successfully created a product';
//     });
//   } else {
//     return Promise.reject('Not authorized to add a product on this page');
//   }
// };

// module.exports.createProduct = (data) => {

// 	console.log(data);

// 	if(data.isAdmin){

// 		let newProduct = new Product({
// 			name : data.product.name,
// 			description : data.product.description,
// 			price : data.product.price
// 		});

// 		console.log(newProduct);
// 		return newProduct.save().then((product, error) => {

// 		if (error) {

// 			return false;

// 		} else {

// 			return product;

// 		};

// 	});

// 	}else{
// 		return false;
// 	};
// };

//Retrieve all active products

module.exports.getAllActive =()=>{
	return Product.find({isActive: true}).then(result=>{
			return result
})}

//Retrieve all products (active and not active)

module.exports.getAllProduct =()=>{
	return Product.find({}).then(result=>{
			return result
})}

//Retrieve single product

module.exports.getProduct = (reqParams)=>{
	return Product.findById(reqParams.productId).then(result=>{
		return result;
	});
};

//Update Product information (Admin only)

module.exports.updateProduct = (reqParams,reqBody) =>{
	let updatedProduct ={
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};
	return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error)=>{
		if(error){
			return false;
		}else{
			return "All updated!";
		};
	});
};

//Archive Product (Admin only)

module.exports.archiveProduct = (reqParams,reqBody) =>{
	let archiveProduct ={
		isActive : reqBody.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId,archiveProduct).then((product,error)=>{
		if(error){
			return false;
		}else{
			return { message: "Product is now archived!" }
		};
	});
};

//Activate Product (Admin only)

module.exports.activateProduct = (reqParams,reqBody) =>{
	let activateProduct ={
		isActive : reqBody.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId,activateProduct).then((product,error)=>{
		if(error){
			return false;
		}else{
			return {message: "Product is now available and on stock"};
		};
	});
};



