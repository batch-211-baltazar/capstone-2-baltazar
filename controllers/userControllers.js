const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//User Registration

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((result,error)=>{
		if(error){
			return false;
		}else{
			return 'Successfully registered, Welcome!';
		};
	});
};

//User Authentication

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {
					userId: result._id.valueOf(),
				access: auth.createAccessToken(result)}
			}else{ 
				return false
			};
		};
	});
};

//Make User Admin

module.exports.makeAdmin = (data) =>{
	return User.findById(data.userId).then(result=>{
		result.isAdmin = true;
		return result.save();
	})
}

//Non-admin User checkout (Create Order)

module.exports.getOrders = async (data) =>{
	console.log(data)
	let isUserUpdated = await User.findById(data.userId).then(user=>{

		user.orders.push({products:[{quantity:data.quantity,productId:data.productId}],totalAmount:data.totalAmount})


		return user.save().then((user,error)=>{
			//console.log(user)
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});

	let isOrderUpdated = await Product.findById(data.productId).then(product=>{

		product.orders.push({
			userId: data.userId,
			quantity: data.quantity			

	});

		return product.save().then((product,error)=>{

			var orders = product.orders;

			for(var x = 0; x < orders.length; x++){
				var order = orders[x];
				order.orderId = order.id;
			}
			console.log(orders);

		return product.save().then((res,error)=>{
		 	console.log("RESPONSE AFTER UPDATE: ", res)
				if(error){
				return false;
			}else{
				return true;
			};
			
		})
			
		});
	});


	if(isUserUpdated&&isOrderUpdated){
		return true;
	}else{
		return false;
	};
};





//Retrieve User Details

module.exports.getProfile = (data) =>{
	return User.findById(data.userId).then(result=>{
		return result;
	})
}



//Retrieve all orders (Admin only)

module.exports.getAllOrders = () =>{
	return User.find({}, {"orders":1, _id:0}).then(result=>{
		return result
	})
}

