const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({
	userId: {
		type: String
			},
	totalAmount: {
		type: Number
				},
purchasedOn: {
	type: Date,
	default: new Date()
			},
products: [
{
productId: {
type: string
}
},
quantity : {
type: Number
}
]
});
