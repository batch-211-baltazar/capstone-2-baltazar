const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

//Route for Create Product (Admin only)

// router.post('/create',auth.verify, (req, res)=>{

//     const isAdmin = auth.decode(req.headers.authorization).isAdmin;

//     productController.createProduct(req.body,isAdmin)
//        .then(result => res.send(result))
//        .catch(error => res.send(error));

// })

router.post("/",auth.verify,(req,res)=>{

	const data ={
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	productController.createProduct(data).then(resultFromController=>res.send(resultFromController))
});

//Route to Retrieve all active products

router.get("/active",(req,res)=>{
	productController.getAllActive().then(resultFromController=>res.send(resultFromController))
});

//Route to Retrieve all products

router.get("/allProducts",(req,res)=>{
	productController.getAllProduct().then(resultFromController=>res.send(resultFromController))
});


//Route to Retrieve single product

router.get("/:productId",(req,res)=>{
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController));
})

//Route to Update Product information (Admin only)

router.put("/:productId",auth.verify,(req,res)=>{
	productController.updateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

//Archive Product (Admin only)

router.put("/:productId/archive",auth.verify,(req,res)=>{
	productController.archiveProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

//Route to activate product (admin only)

router.put("/:productId/activate",auth.verify,(req,res)=>{
	productController.activateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});




module.exports = router;