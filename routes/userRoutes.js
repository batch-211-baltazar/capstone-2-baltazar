const express = require("express");
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require("../auth");

//Route for User Registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//Route for User Authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
});

//Route for Making User Admin

router.put("/updateAdmin/:id",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.makeAdmin({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});


//Route for Non-admin User checkout (Create Order)


router.post("/order",auth.verify,(req,res)=>{

	var quantity = req.body.quantity ? req.body.quantity :1;
	let data = {
		userId: req.body.userId,
		productId: req.body.productId,
		quantity: req.body.quantity,
		totalAmount: req.body.price * quantity
		
	}


	userController.getOrders(data).then(resultFromController=>res.send(resultFromController));
})

//Route for Retrieving User Details

router.get("/UserDetails",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});

// router.get("/all",(req,res)=>{
// 	userController.getAllOrders().then(resultFromController=>res.send(resultFromController))
// });

router.get("/allOrders",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getAllOrders({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});


module.exports = router;